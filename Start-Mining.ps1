﻿

$mypath=Split-Path $MyInvocation.MyCommand.Path

$VerbosePreference = 'Continue'

.\Ping-Reset.ps1

#TODO: Attach to process and listen to it
$proc=$proc=Get-Process -Name (Split-Path $current_miner -Leaf)
if($proc)
{
    #found existing process, killing
    $proc.kill()
    $proc.waitforexit(1000)
}

#get cards
$cards=Get-WmiObject Win32_PNPEntity -Filter "PNPClass='Display' AND Name='Radeon RX Vega'"

#restart gpus
#$cards|%{$_.disable();sleep 1; $_.enable(); sleep 1}
$switchRadeon=join-path $mypath cast_xmr\switch-radeon-gpu.exe -Resolve
&$switchRadeon --hbcc off --largepages on --admin fullrestart

& "$mypath\Overdrive-Apply.ps1"



$pool="europe.cryptonight-hub.miningpoolhub.com:20580"
$wallet= if($env:COMPUTERNAME -eq "DESKTOP-MINER-1"){'narciss.4'}else{'narciss.asrock'} 
$pw='x'

$gpu_arg=0..($cards.Length-1) -join ','

$miners=@{
    #start cast-xmr with all gpus    
    cast_xmr="$mypath\cast_xmr\cast_xmr-vega.exe"
    cast_xmr_args=("--reconnects 3 --intensity 9 --nonicehash -R -S $pool -u $wallet -p $pw -G $gpu_arg" -split ' ')
    #TODO: Write config file for claymore miners
    cryptonote="'$mypath\CryptoNote\NsGpuCNMiner.exe' $mypath\cryptonote.config.txt"
}


$env:GPU_FORCE_64BIT_PTR=1
$env:GPU_MAX_HEAP_SIZE=100
$env:GPU_USE_SYNC_OBJECTS=1
$env:GPU_MAX_ALLOC_PERCENT=100
$env:GPU_SINGLE_ALLOC_PERCENT=100

$current_miner=$miners.cast_xmr
$current_miner_args=$miners.cast_xmr_args

.\Ping-Reset.ps1

while ($true)
{
    #Invoke-Expression "& $current_miner 2>&1 > miner_$_.log"
    #TODO: Ping cast-xmr for disconnected, then restart
    $proc=Start-Process -FilePath $current_miner -ArgumentList $current_miner_args -RedirectStandardOutput miner.log -RedirectStandardError miner.err -PassThru -Verbose:$true
    sleep 15
    while ($true)
    {
        $json=$false
        #Check for: Process responds with rest, still there
        $json=Invoke-RestMethod -Method GET http://localhost:7777
        if ($json)
        {
            .\Ping-Reset.ps1
            Write-host ("{0:G}: Status of pool: $($json.pool.status)" -f [DateTime]::Now)
            $json.devices|%{"{0}: {1:N0} khs" -f $_.device,($_.hash_rate/1000)}
            if ($json.pool.status -match 'disconnected')
            {
                Write-host "Disconnect detected, restarting miner"
                $proc.Kill()
                $proc.WaitForExit(5000)
                Write-host "Miner killed, restarting"
                break;
            }
        } else {
            #no ping
        }
        
        sleep 10
    }
    
    #$json=Invoke-RestMethod -Method GET http://localhost:7777
    #$status.pool.status = connected|disconnected
}


#TODO: Reboot!





