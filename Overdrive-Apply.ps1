﻿


$mypath=$PSScriptRoot

#thx https://blogs.technet.microsoft.com/heyscriptingguy/2011/08/20/use-powershell-to-work-with-any-ini-file/
function Get-IniContent ($filePath)
{
    $ini = @{}
    switch -regex -file $FilePath
    {
        "^\[(.+)\]" # Section
        {
            $section = $matches[1]
            $ini[$section] = @{}
            $CommentCount = 0
        }
        "^(;.*)$" # Comment
        {
            $value = $matches[1]
            $CommentCount = $CommentCount + 1
            $name = "Comment" + $CommentCount
            $ini[$section][$name] = $value
        } 
        "(.+?)\s*=(.*)" # Key
        {
            $name,$value = $matches[1..2]
            $ini[$section][$name] = $value
        }
    }
    return $ini
}

$overdrive_keys=[ordered]@{
    'SUBSYS_0B361002&REV_C1'='AMD_64'
    'SUBSYS_36811462&REV_C3'='MSI_56'
    'SUBSYS_E37F1DA2&REV_C3'='Nitro_56'
    'SUBSYS_E37F1DA2&REV_C1'='Nitro_64'
    'SUBSYS_23081458&REV_C1'='Gigabyte_64'
    }

#get cards
$cards=Get-WmiObject Win32_PNPEntity -Filter "PNPClass='Display' AND Name='Radeon RX Vega'"

$matcher=[regex]"\d+,\d+,\d+"
$byBusID=@{}
foreach ($card in $cards)
{
    $locationInfo = (get-itemproperty -path "HKLM:\SYSTEM\CurrentControlSet\Enum\$($card.PNPDeviceID)" `
        -name locationinformation).locationINformation    
    if($matcher.IsMatch($locationInfo))
    {
        $busId,$deviceID,$functionID = $matcher.Match($locationInfo).value -split "," 
        $byBusID[[int]$busId]=$card
        Write-Host "Found card $locationInfo"
    } else 
    {
        Write-Warning -Message "Couldnt extract bus info from $locationInfo"
    }
    
}
#Make sure its sorted
$byBusID=$byBusID.GetEnumerator()|sort Name

$subsysRevID=[regex]'(SUBSYS_[^\&]+&REV_\w{2,2})'
#apply overdrive
$idx=1

$overdrive_args_reset=@()
$overdrive_args=@()
$overdrive_args_reapply=@()

$iniContent=Get-IniContent((Join-Path $PSScriptRoot '.\OverdriveNTool\OverdriveNTool.ini'))
$byProfile=$iniContent.keys|?{$_ -match 'Profile_*'}|%{New-Object pscustomobject -Property $iniContent[$_]}|Group-Object -Property Name -AsHashTable
foreach($entry in $byBusID)
{
    $devID=$entry.value.deviceid
    $key=$subsysRevID.Match($devID).Groups[1].value
    $profile=$overdrive_keys[$key]
    $overdrive_args_reset += "-r$idx"
    $overdrive_args += "-p$idx`"$profile`""
    #Also set values manually once more, bug in overdriventool
    #e.g. "-ac1" "GPU_P7=1392;963" "-ac2" "GPU_P7=1320;995" "-ac3" "GPU_P7=1392;963" 
    #now get the raw values
    $p7State = $byProfile[$profile].GPU_P7 -split ';'
    $overdrive_args_reapply += "-ac$idx"
    $overdrive_args_reapply += "GPU_P7=" + (([int]$p7State[0]) + 1) + ';' + (([int]$p7State[1]) + 1)
    $idx+=1
}

#TODO: Get ini values and apply seperately 


sleep 1
&$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args
sleep 1
&$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args_reapply
#sleep 1
#&$mypath\OverdriveNTool\OverdriveNTool.exe -consoleonly @overdrive_args

